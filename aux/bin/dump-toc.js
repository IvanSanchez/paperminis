const util= require("util");

// import PDFJS from "pdfjs-dist";
const PDFJS = require("pdfjs-dist");

const { JSDOM } = require("jsdom");

const { crc32 } = require("crc");

// const { PNGImage } = require('pnglib-es6');
const sharp = require("sharp");

// Add some globals that exist on a graphical browser, because pdfjs-dist
// expects these globals to exist.
const window = new JSDOM().window;
global.document = window.document;
// global.Image = window.Image;

// console.log(global.document);

if (!process.argv[2]) {
	console.error();
	process.exit(1);
}

let page;
let jpgs = [];
let opsDict = [];
Object.keys(PDFJS.OPS).forEach((opname) => {
	// 	console.log(opname, PDFJS.OPS[opname]);
	opsDict[PDFJS.OPS[opname]] = opname;
});


function cleanupOutlineItem(item) {
    return {
        dest: item.dest,
        title: item.title,
        count: item.count,
        items: ((typeof item.dest !== 'string' && item.items) || []).map(cleanupOutlineItem)
    }
}



PDFJS.getDocument({
	url: process.argv[2],
	nativeImageDecoderSupport: "none",
}).then((doc) => {
	console.log("doc: ", doc);

	const knownHashes = [];

	console.log("Page count: ", doc.pdfInfo.numPages);
	console.log("Fingerprint: ", doc.pdfInfo.fingerprint);


    doc.getOutline().then(outline=>{
//         console.log("Outline: ", util.inspect(outline, {depth: null}));
        console.log("Outline: ", util.inspect(outline.map(cleanupOutlineItem), {depth: null}));
    })
    .then(()=>{ return doc.getPageLabels() })
// //     .then((labels)=>{
// //         console.log("Labels: ", util.inspect(labels));
// //     })
// //     .then(()=>{ return doc.getAttachments() })
// //     .then((attachments)=>{
// //         console.log("Attachments: ", util.inspect(attachments));
// //     })
//     .then(()=>{ return doc.getDestinations() })
//     .then((dests)=>{
//         console.log("Destinations: ", util.inspect(dests));
//     })
    ;

});
