// import PDFJS from "pdfjs-dist";
const PDFJS = require("pdfjs-dist");

const { JSDOM } = require("jsdom");

// const { PNGImage } = require('pnglib-es6');
const sharp = require("sharp");

// Add some globals that exist on a graphical browser, because pdfjs-dist
// expects these globals to exist.
const window = new JSDOM().window;
global.document = window.document;
// global.Image = window.Image;

// console.log(global.document);

if (!process.argv[2]) {
	console.error();
	process.exit(1);
}

let page;
let jpgs = [];
let opsDict = [];
Object.keys(PDFJS.OPS).forEach((opname) => {
	console.log(opname, PDFJS.OPS[opname]);
	opsDict[PDFJS.OPS[opname]] = opname;
});

const ignoredOps = [
	1, // dependency: 1
	2, // setLineWidth: 2
	3, // setLineCap: 3
	4, // setLineJoin: 4
	5, // setMiterLimit: 5
	6, // setDash: 6
	7, // setRenderingIntent: 7
	8, // setFlatness: 8
	9, // setGState

	// 	10, // save
	// 	11, // restore
	//
	// 	12, // transform
	//
	// 	20, // stroke
	// 	21, // closeStroke
	// 	22, // fill
	// 	23, // eoFill
	// 	24, // fillStroke
	// 	29, // clip

	31, // beginText
	32, // endText

	33, // setCharSpacing
	34, // setWordSpacing
	35, // setHScale
	36, // setLeading
	37, // setFont
	38, // setTextRenderingMode
	39, // setTextRise
	40, // moveText
	41, // setLeadingMoveText
	42, // setTextMatrix
	43, // nextLine
	44, // showText
	58, // setStrokeRGBColor
	59, // setFillRGBColor

	// 	91, // constructPath
];

const indenters = [
	10, // save
	31, // beginText
	91, // constructPath
	74, // paintFormXObjectBegin
	76, // beginGroup
];

const unindenters = [
	11, // restore

	20, // strike
	21, // closeStroke
	22, // fill
	23, // eoFill
	24, // fillStroke
	28, // endPath

	32, // endText
	75, // paintFormXObjectEnd
	77, // endGroup
];

// Display the arguments for the following operations
const verboseOps = [
	85, // paintImageXObject

	78, // beginAnnotations
	79, // endAnnotations
];

PDFJS.getDocument({
	url: process.argv[2],
	nativeImageDecoderSupport: "none",
}).then((doc) => {


// getAttachments: ƒ getAttachments()
// getData: ƒ getData()
// getDestination: ƒ getDestination(id)
// getDestinations: ƒ getDestinations()
// getDownloadInfo: ƒ getDownloadInfo()
// getJavaScript: ƒ getJavaScript()
// getMetadata: ƒ getMetadata()
// getOpenActionDestination: ƒ getOpenActionDestination()
// getOutline: ƒ getOutline()
// getPage: ƒ getPage(pageNumber)
// getPageIndex: ƒ getPageIndex(ref)
// getPageLabels: ƒ getPageLabels()
// getPageLayout: ƒ getPageLayout()
// getPageMode: ƒ getPageMode()
// getPermissions: ƒ getPermissions()
// getStats: ƒ getStats()
// getViewerPreferences: ƒ getViewerPreferences()
// loadingParams: (...)

	console.log("Page count: ", doc.numPages);
	console.log("Fingerprint: ", doc.fingerprint);

	doc.getAttachments().then((atts)=>{
		console.log('attachments', atts);
	})
	.then(()=>doc.getMetadata())
	.then((meta)=>{
			console.log('meta', meta);
	})
	.then(()=>doc.getDestinations())
	.then((dests)=>{
			console.log('dests', dests);
	})
	.then(()=>doc.getStats())
	.then((stats)=>{
			console.log('stats', stats);
	})
	.then(()=>doc.getJavaScript())
	.then((js)=>{
			console.log('js', js);
	})
	.then(()=>doc.getOpenActionDestination())
	.then((openaction)=>{
			console.log('openaction', openaction);

			if (openaction[1] && openaction[1].name) {
				return doc.getDestination(openaction[1].name).then((dest)=>{
					console.log('open destination: ', openaction[1].name, dest);
				}
				)
			}

	})
	.then(()=>{

	doc.getPage(1).then((page) => {
        console.log("Page number: ", page.pageNumber);
        console.log("Page ref: ", page.ref);
        console.log("Page stata: ", page.stats);

		page.getOperatorList().then((ops) => {
			let indentation = 0;
			let skippedOps = 0;
console.log(ops.fnArray);
			for (let i = 0, l = ops.fnArray.length; i < l; i++) {
				const fnCode = ops.fnArray[i];

				if (!ignoredOps.includes(fnCode)) {
					// 	if (skippedOps) {
					// 		console.log(
					// 			"".padStart(indentation) + "(skipped",
					// 			skippedOps,
					// 			"ops)"
					// 		);
					// 	}

					if (unindenters.includes(fnCode)) {
						indentation--;
					}

					console.log(
						"".padStart(indentation) + "op: ",
						opsDict[fnCode],
						fnCode,
						verboseOps.includes(fnCode) ? ops.argsArray[i] : ""
					);

					if (indenters.includes(fnCode)) {
						indentation++;
					}
				} else {
					skippedOps++;
				}
			}
		});
	});
	});
});
