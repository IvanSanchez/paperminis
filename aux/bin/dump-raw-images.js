// import PDFJS from "pdfjs-dist";
const PDFJS = require("pdfjs-dist");

const { JSDOM } = require("jsdom");

const { crc32 } = require("crc");

// const { PNGImage } = require('pnglib-es6');
const sharp = require("sharp");

// Add some globals that exist on a graphical browser, because pdfjs-dist
// expects these globals to exist.
const window = new JSDOM().window;
global.document = window.document;
// global.Image = window.Image;

// console.log(global.document);

if (!process.argv[2]) {
	console.error();
	process.exit(1);
}

let page;
let jpgs = [];
let opsDict = [];
Object.keys(PDFJS.OPS).forEach((opname) => {
	// 	console.log(opname, PDFJS.OPS[opname]);
	opsDict[PDFJS.OPS[opname]] = opname;
});

PDFJS.getDocument({
	url: process.argv[2],
	nativeImageDecoderSupport: "none",
}).then((doc) => {
	console.log("doc: ", doc);

	const knownHashes = [];

	console.log("Page count: ", doc.pdfInfo.numPages);
	console.log("Fingerprint: ", doc.pdfInfo.fingerprint);

	let serialPromise = Promise.resolve();

	for (
		let pageNumber = 1, pageCount = doc.pdfInfo.numPages;
		pageNumber <= pageCount;
		pageNumber++
	) {
		console.log("Queueing page: ", pageNumber);
		serialPromise = serialPromise
		.then(()=>new Promise(res=>{setTimeout(res, 500)}))
		.then(()=>doc.getPage(pageNumber))
        .then((page) => {
			page.getOperatorList().then((ops) => {
				let indentation = 0;
				let skippedOps = 0;

				for (let i = 0, l = ops.fnArray.length; i < l; i++) {
					if (ops.fnArray[i] === 85) {
						const [imageName, w, h] = ops.argsArray[i];

						const image = page.objs.get(imageName);

						const bytes = Uint8Array.from(image.data);
						const hash = crc32(bytes);
						//                     console.log("Image hash is 0x" + hash.toString(16));

						if (knownHashes.includes(hash)) {
							console.log("Image already used before, skipping");
						} else {
							console.log({
								name: imageName,
								width: image.width,
								height: image.height,
								kind: image.kind,
							});
							knownHashes.push(hash);

							// "kind" marks the bit depth. See
							// https://github.com/mozilla/pdf.js/blob/b56081c5f89c0602151df149a622876bac8c21d4/src/shared/util.js#L41
							if (image.kind === 2) {
								// 24-bit RGB
								const sharpImage = new sharp(Buffer.from(bytes), {
									raw: {
										width: image.width,
										height: image.height,
										channels: 3,
									},
								});
								console.log("Writing /tmp/" + imageName + ".png");
								sharpImage.toFile("/tmp/" + imageName + ".png");
							} else if (image.kind === 3) {
								// 32-bit RGBA
								const sharpImage = new sharp(Buffer.from(bytes), {
									raw: {
										width: image.width,
										height: image.height,
										channels: 4,
									},
								});
								console.log("Writing /tmp/" + imageName + ".png");
								sharpImage.toFile("/tmp/" + imageName + ".png");
							} else {
								console.warn("Unrecognized image colour depth");
							}
						}
					}
				}
			});
		});
	}
	return serialPromise;
});
