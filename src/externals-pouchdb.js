// This file is here to make Rollup bundle PDFJS in a separate file,
// just to make the build smaller & faster
// (Without this, the whole non-minified build clocks in at 2.7MiB).

import PouchDB from "./../node_modules/pouchdb/dist/pouchdb";

export default PouchDB;
