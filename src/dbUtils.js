// A few functions for handling DB stuff that got repeated elsewhere in the code,
// and got refactored here.

// Kinda messed up with recordUtils.mjs

const imagesDb = new PouchDB("images");

const thumbnailBlobCache = {};
const fullImageBlobCache = {};

export function updateCreature(imageId, creatureId, changes) {
	return imagesDb
		.get(imageId)
		.then((doc) => {
			const creatures = doc.creatures;

			if (creatureId === undefined) {
				creatureId = doc.nextCreatureId++;
				creatures[creatureId] = changes;
			} else {
				creatures[creatureId] = { ...creatures[creatureId], ...changes };
			}

			const imageChanges = {
				creatures,
				nextCreatureId: doc.nextCreatureId,
			};

			// 			console.log("updating creature:", { ...doc, ...imageChanges });

			return imagesDb
				.put({ ...doc, ...imageChanges })
				.then(() => creatureId)
				.catch((err) =>
					console.error(
						`Database update of creature ${imageId}/${creatureId} failed with ${err}`
					)
				);
		})
		.catch((err) =>
			console.error(`Database fetch of image ${imageId} failed with ${err}`)
		);
}

export function deleteCreature(imageId, creatureId) {
	return imagesDb
		.get(imageId)
		.then((doc) => {
			const creatures = doc.creatures;
			delete creatures[creatureId];

			return imagesDb
				.put({ ...doc, creatures })
				.then(() => creatureId)
				.catch((err) =>
					console.error(
						`Database deletion of creature ${imageId}/${creatureId} failed with ${err}`
					)
				);
		})
		.catch((err) =>
			console.error(`Database fetch of image ${imageId} failed with ${err}`)
		);
}

export function updateImage(id, changes) {
	return imagesDb
		.get(id)
		.then((doc) =>
			imagesDb
				.put({ ...doc, ...changes })
				.catch((err) =>
					console.error(
						`Database update of image ${id} failed with ${err}`
					)
				)
		)
		.catch((err) =>
			console.error(`Database fetch of image ${id} failed with ${err}`)
		);
}

// Given an image ID, return a Promise to a string containing the blob: URI for
// its thumbnail, or the string itself if it's already cached.
export function getThumbnailBlob(imageId) {
	if (thumbnailBlobCache[imageId]) {
		return thumbnailBlobCache[imageId];
	} else {
		return imagesDb
			.getAttachment(imageId, "thumbnail")
			.then((data) => {
				return (thumbnailBlobCache[imageId] = URL.createObjectURL(data));
			})
			.catch((err) =>
				console.error(
					`Error loading thumbnail for image id: ${imageId} (${err})`
				)
			);
	}
}

// Like getThumbnailBlob, but for a data structure containing src, width and height of
// the entire original image
export function getFullImageData(imageId) {
	if (fullImageBlobCache[imageId]) {
		return Promise.resolve(fullImageBlobCache[imageId]);
	} else {
		return (fullImageBlobCache[imageId] = imagesDb
			.getAttachment(imageId, "image")
			.then((data) => URL.createObjectURL(data))
			.then((src) => {
				return new Promise(function(resolve, reject) {
					const img = new Image();
					img.onload = function() {
						resolve({
							width: img.naturalWidth,
							height: img.naturalHeight,
							src,
						});
					};
					img.onerror = reject;
					img.src = src;
				});
			})
			.catch((err) =>
				console.error(
					`Error loading full image for image id: ${imageId} (${err})`
				)
			));
	}
}

// Gets a `Set` of books, fetched from the images
export function getBooks() {
	return imagesDb
		.allDocs({
			include_docs: true,
		})
		.then((docs) => new Set(docs.rows.map((r) => r.doc.fileinfo.name)));
}

// Gets a `Set` of tags, fetched from the images
export function getImageTags() {
	return imagesDb
		.allDocs({
			include_docs: true,
		})
		.then((docs) => new Set(docs.rows.map((r) => r.doc.tags).flat()));
}
