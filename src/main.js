import App from "./App.svelte";

const app = new App({
	target: document.body,
});

// Remove the static splash info (meant for crawlers) once everything loads.
document.body.removeChild(document.getElementById("static-splash"));

export default app;
