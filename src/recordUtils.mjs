// Some generic component-agnostic functionality regarding image records
// Mostly copy-pasted away from the UploadImage component

// Kinda messed up with dbUtils.mjs

const db = new PouchDB("images");

const thumbnailSize = 200;

// Given a blob of an image, returns a promise for the blob of its thumbnail.
function createThumbnail(blob) {
	return new Promise(function(resolve, reject) {
		const orig = new Image();
		orig.onload = function() {
			let scale = Math.min(
				thumbnailSize / orig.height,
				thumbnailSize / orig.width
			);

			if (scale > 1) {
				// No thumbnail needed for this size. Reusing the original
				// image is tricky due to side effects - therefore, create
				// a thumbnail the same size as the original image.
				scale = 1;
			}

			const canvas = document.createElement("canvas");

			canvas.width = Math.max(1, orig.width * scale);
			canvas.height = Math.max(1, orig.height * scale);

			canvas
				.getContext("2d")
				.drawImage(orig, 0, 0, canvas.width, canvas.height);

			return canvas.toBlob((blob) => {
				resolve({ blob, width: orig.width, height: orig.height });
			});
		};
		orig.onerror = reject;
		orig.src = URL.createObjectURL(blob);
	});
}

// Returns a Promise to a crypto hash of the bytes of the given blob.
function cryptoHash(bytes) {}

// Given a POJO with the information about the image (which should have
// the same fields as the base image record) and an image blob, save it
// to the DB.
// Returns a Promise which resolves to an info string or rejects with an error string.
export function addImageToDatabase(imageInformation, image) {
	return createThumbnail(image.data).then((thumbnailData) => {
		const sanitizedInfo = {
			fileinfo: {
				type: "",
				name: "",
				page: 0,
				pageName: "",
			},
			name: "",

			// Hashes of the image, and of any duplicates merged
			/// FIXME
			hashes: [], // SHA-256 hash of the image bytes

			/// FIXME
			phash: "", // Perceptual hash
			tags: [],
			creatures: [],
			nextCreatureId: 1,

			// mimetype of individual image, "ImageXObject", "InlineImageXObject" or "JpegXObject"
			original_content_type: "",

			width: thumbnailData.width,
			height: thumbnailData.height,
			_attachments: {
				image,
				thumbnail: {
					content_type: thumbnailData.blob.type,
					data: thumbnailData.blob,
				},
			},
			...imageInformation,
		};

		return db
			.post(sanitizedInfo)
			.then(
				(posted) =>
					`Sent to DB: ${sanitizedInfo.fileinfo.name} ${sanitizedInfo.name}`
			)
			.catch((err) => `Could not send image to DB: ${err}`);
	});
}
