// This taxonomy exists only as a *helper* for the sequence tagger, and for the
// autocompletion suggestions on the tag editor. It is not meant to be complete
// or authoritative.

// Game or maybe setting or maybe game system or maybe publisher
export const games = {
	trigger: () => true,
	opts: ["pathfinder", "starfinder", "gurps", "dnd"],
};

// Broad categories, paperpawner should only worry about creatures and ignore the rest
export const imageContents = {
	trigger: () => true,
	opts: [
		"creature",
		"portrait",
		"illustration",
		"vehicle",
		"symbol",
		"item",
		"map",
	],
};

export const symbolType = {
	trigger: (a) => a.includes("symbol"),
	opts: ["religion-symbol", "faction-symbol", "place-symbol", "class-symbol"],
};

export const vehicleType = {
	/// TODO: in theory, a vehicle can have multiple areas of applications, e.g. amphibious
	trigger: (a) => a.includes("vehicle"),
	opts: ["ground-vehicle", "spacecraft", "aircraft", "seacraft"],
};

// Creature types for pathfiner/starfinder
export const paizoCreatureTypes = {
	trigger: (a) =>
		(a.includes("starfinder") || a.includes("pathfinder")) &&
		a.includes("creature"),
	opts: [
		"aberration",
		"animal",
		"beast", // Or either "magical-beast"/"monstrous-humanoid" in 1st ed
		"construct",
		"dragon",
		"fey",
		"humanoid",
		// 		"magical-beast",	// Or just "beast" in 2nd ed
		// 		"monstrous-humanoid",	// Or just "beast" in 2nd ed
		"ooze",
		"outsider",
		"plant",
		"undead",
		// 		"vermin",	// Or animal → insect in 2nd ed
	],
};

// Humanoid PC/NPC races/ancestries for the pathfinder RPG
export const pathfinderHumanoidRaces = {
	trigger: (a) => a.includes("pathfinder") && a.includes("humanoid"),
	opts: [
		"dwarf",
		"elf",
		"gnome",
		"goblin",
		"halfling",
		"half-elf",
		"half-orc",
		"human",
		// goblins are AKA goblinoids

		// planas scions: half-celestial, -fiend or -monitor
		// urobians are AKA duskwalkers
		"aasimar",
		"tiefling",
		"urobian",

		// Darklands AKA underworld. Xulgath are AKA troglidites
		"dero",
		"drow",
		"duergar",
		"xulgath",

		"boggard",
		"catfolk",
		"caligni",
		"changeling",
		"dhampir",
		"giant",
		"gnoll",
		"hag",
		"kobold",
		"lizardfolk",
		"merfolk",
		"orc",
		"ratfolk",
		"sea-devil",
		"tengu",
		"werecreature",
	],
};

// Humanoid PC/NPC races for the starfinder RPG
export const starfinderHumanoidRaces = {
	trigger: (a) => a.includes("starfinder") && a.includes("humanoid"),
	opts: ["android", "human", "kasatha", "lashunta", "shirren", "vesk", "ysoki"],
};

// Humanoid PC/NPC classes for the starfinder RPG
export const starfinderClasses = {
	trigger: (a) => a.includes("starfinder") && a.includes("humanoid"),
	opts: [
		"envoy",
		"mechanic",
		"mystic",
		"operative",
		"solarian",
		"soldier",
		"technomancer",
	],
};

// Humanoid PC/NPC classes for the (core 2nd ed) pathfinder RPG
export const pathfinderClasses = {
	trigger: (a) => a.includes("pathfinder") && a.includes("humanoid"),
	opts: [
		"alchemist",
		"barbarian",
		"bard",
		"champion",
		"cleric",
		"druid",
		"fighter",
		"monk",
		"ranger",
		"rogue",
		"sorcerer",
		"wizard",
	],
};

// Human ethnicities for the Golarion setting of the (core 2nd ed) pathfinder RPG
export const pathfinderHumanEthnicities = {
	trigger: (a) => a.includes("pathfinder") && a.includes("humanoid"),
	opts: [
		"garundi",
		"kelleshite",
		"kellid",
		"mwangi",
		"nidalese",
		"taldan",
		"tian",
		"ulfen",
		"varisian",
		"vudrani",
	],
};

// Creature subtypes for the (core 2nd ed) pathfinder RPG
// export const creatureSubtypesAberration = {
// 	trigger: (a) => a.includes("pathfinder") && a.includes("aberration"),
// 	opts: [
// 		"",
// 	],
// }

export const creatureSubtypesAnimal = {
	trigger: (a) => a.includes("pathfinder") && a.includes("animal"),
	opts: [
		"avian",
		"dinosaur",
		"fish",
		"insect", // Includes arachnids
		"mammal",
		"reptile",
	],
};

// export const creatureSubtypesBeast = {
// 	trigger: (a) => a.includes("pathfinder") && a.includes("beast"),
// 	opts: [
//		"monstrous-humanoid",
//		"magical-beast",
//		"dragon",	// Seems logical to group dragons here too.
//		"fiend",	// Some creatures are listed as beast fiends (instead of outsider fiends)
// 	],
// }

export const creatureSubtypesConstruct = {
	trigger: (a) => a.includes("pathfinder") && a.includes("construct"),
	opts: [
		"golem",
		"inevitable", // Created by axiomites
		"soulbound",
	],
};

// export const creatureSubtypesDragon = {
// 	trigger: (a) => a.includes("pathfinder") && a.includes("dragon"),
// 	opts: [
// 		"",
// 	],
// }

export const creatureSubtypesFey = {
	trigger: (a) => a.includes("pathfinder") && a.includes("fey"),
	opts: ["gremlin", "nymph", "sprite"],
};

// export const creatureSubtypesOoze = {
// 	trigger: (a) => a.includes("pathfinder") && a.includes("ooze"),
// 	opts: [
// 		"",
// 	],
// }

export const creatureSubtypesOutsider = {
	trigger: (a) => a.includes("pathfinder") && a.includes("outsider"),
	opts: [
		"celestial", // Good
		"fiend", // Evil
		"monitor", // Neutral

		"astral",
		"elemental",
		"ethereal",
		"genie",
	],
};

export const creatureSubtypesCelestial = {
	trigger: (a) => a.includes("pathfinder") && a.includes("celestial"),
	opts: [
		"angel", // NG
		"archon", // LG
		"azata", // CG
	],
};
export const creatureSubtypesFiend = {
	trigger: (a) => a.includes("pathfinder") && a.includes("fiend"),
	opts: [
		"demon", // CE
		"daemon", // NE
		"devil", // LE

		"rakshasa", // reincarnated evil souls
	],
};
export const creatureSubtypesMonitor = {
	trigger: (a) => a.includes("pathfinder") && a.includes("monitor"),
	opts: [
		"aeon", // LN
		"protean", // CN
		"psychopomp", // NN
	],
};

export const creatureSubtypesPlant = {
	trigger: (a) => a.includes("pathfinder") && a.includes("plant"),
	opts: ["fungus", "leshy"],
};

export const creatureSubtypesUndead = {
	trigger: (a) => a.includes("pathfinder") && a.includes("undead"),
	opts: [
		"ghost", // Should be undead → spirit → ghost
		"ghoul",
		"mummy",
		"skeleton",
		"spirit",
		"vampire",
		// 		"wight",
		// 		"wraith",
		"zombie",
	],
};

// export const creatureSubtypesAnimal = {
// 	trigger: (a) => a.includes("pathfinder") && a.includes("vermin"),
// 	opts: [
// 		"",
// 	],
// }

// // Starfinder constructs
// export const starfinderConstructs = {
// 	trigger: (a)=>a.includes('starfinder') && a.includes('construct'),
// 	opts: ['robot', 'drone']
// }
