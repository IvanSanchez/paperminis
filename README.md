# paperpawner

A tool to create wargaming-style paper miniatures

It's live at https://ivan.sanchezortega.es/paperpawner

## What

This tool eats PDFs, and helps turn the images contained in them into Paizo-style 28mm-scale paper miniatures (for use with boardgames, wargaming or tabletop RPGs).

It's most useful for people wanting to create lots of cheap-looking miniatures instead of tracking down metal/plastic minis.

**paperpawner** runs **only** on your web browser: it does **not** transmit information anywhere. There is no web server in any magical cloud storing any images (or any information about any images) at all.

Image extraction from PDFs is done at a low-level, by looking at the PDF opcodes. It works as long as the PDFs are not rasterized (each page becomes one impossible-to-split image). I have found some cases of weird [typesetting](https://en.wikipedia.org/wiki/Typesetting)/[DTPing](https://en.wikipedia.org/wiki/Desktop_publishing)/[layouting](https://en.wikipedia.org/wiki/Page_layout) where images are cut into smaller tiles (which are *hard* to put together). I guess there are some cases of oversights when setting output options (and very few cases of [*beep*](https://en.wikipedia.org/wiki/Bleep_censor)hole designers who explicitly try to avoid image extraction from PDFs, but that's a real minority).

## Who

This was made by [Iván Sánchez Ortega](https://ivan.sanchezortega.es), a long-time [AD&D](https://en.wikipedia.org/wiki/Editions_of_Dungeons_%26_Dragons#Advanced_Dungeons_&_Dragons), [Pathfinder](https://paizo.com/pathfinder) & [Delta Green](http://www.delta-green.com/) [GM](https://en.wikipedia.org/wiki/Gamemaster).

**paperpawner** is an *opinionated* tool, one that works for *my* use case and allows me to create paper minis *quickly*. The UI/UX can feel clunky as a result.

Kudos to [vehrka](https://www.vehrka.net/) for putting up with me during my Pathfinder-heavy years, and Per Kristian Shanke "PK" (who games at [Hexagon Spillklubb](https://hexagonspillklubb.wordpress.com/)) for feedback & help on the early versions of paperpawner.

## Why

I don't like metal or plastic miniatures. I have GM'd a lot of game sessions, and having just the right kind of miniature for the moment is *hard*. Getting a collection big enough to cover most cases is expensive, and hard to maintain.

I was most impressed when I got my hands on [one of the first iterations of Paizo's paper miniatures](https://paizo.com/products/btpy8p3w) - affordable monsters and characters to print, fold and glue together. I *loved* the concept.

The downside of metal/plastic minis is kinda moot today since Paizo now offers [affordable cardboard pawns](https://paizo.com/store/pathfinder/accessories/pawns) - but still, availability in european stores is limited, and (more importantly for me) some monsters/NPCs depicted in some adventures are not available anywhere.

The case in point is [The Sanos Abduction](https://paizo.com/products/btpy8u8r?Pathfinder-Society-Scenario-4-05-The-Sanos-Abduction), which features a zombie pegasus in the front cover. Up to date, there aren't any minis or pawns of zombie pegasi that I could find. (Several other Pathfinder Society modules suffer from this - there is wonderful art of a monster or NPC, but no pawn or mini to be found. The use case is a niche one, but it's there!)

**paperpawner** to the rescue! Take the PDF for that adventure, and with a few clicks paperpawner will turn the images into printable paper miniatures:

![Diagram showing high-level workflow](./public/readme-paperpawner-workflow.png)

Once **paperpawner** has doen its work, you should cut, fold and glue the minis as per [the Paizo paper mini instructions](https://paizo.com/products/btpy87p2?Pathfinder-Paper-Minis-Instructions).

## Legalese

The code of paperpawner itself is under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html). See LICENSE.md.

Note that although paperpawner makes use of [localStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage) technology (to hold the images and information about them), the information stored there is never transmitted anywhere.

It should go without saying that this tool is intended for personal use, and/or within a small gaming group, to aid in the creation of gaming aids. But I'll be explicit anyway and say: paperpawner shall not be used to facilitate redistribution of artwork.

Uses Mozilla's PDFJS (under Apache2 license). See https://mozilla.github.io/pdf.js/

Uses PouchDB (under Apache2 license). See https://pouchdb.com/

Built with Svelte. See https://svelte.dev/

The `Human_outline.svg` image is a trivial modification of https://commons.wikimedia.org/wiki/File:Human_outline.svg (under CC-by-sa license).

