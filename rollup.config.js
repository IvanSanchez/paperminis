import fs from "fs";
import svelte from "rollup-plugin-svelte";
import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
// import json from "@rollup/plugin-json";
// import buble from "rollup-plugin-buble";
// import uglify from "rollup-plugin-uglify";
// import ignore from "rollup-plugin-ignore";
// import image from "rollup-plugin-image";
import fileAsBlob from "rollup-plugin-file-as-blob";
// import { string } from "rollup-plugin-string";
import gitVersion from 'rollup-plugin-git-version'
import json from '@rollup/plugin-json'


// This cannot be bundled, so let's copy it directly and worry about it later
// fs.copyFile(
// 	"node_modules/bulma/css/bulma.min.css",
// 	"public/bulma.min.css",
// 	() => {}
// );
fs.copyFile(
	"node_modules/bootstrap/dist/css/bootstrap.min.css",
	"public/bootstrap.min.css",
	() => {}
);
// fs.copyFile(
// 	"node_modules/bootstrap/dist/css/bootstrap.css",
// 	"public/bootstrap.css",
// 	() => {}
// );


// fs.copyFile('node_modules/pdfjs-dist/build/pdf.worker.js', 'public/pdf.worker.js', ()=>{});
// fs.copyFile('node_modules/pdfjs-dist/build/pdf.worker.min.js', 'public/pdf.worker.min.js', ()=>{});

const production = !process.env.ROLLUP_WATCH;

export default [
	{
		input: "src/main.js",
		output: {
			sourcemap: true,
			format: "iife",
			name: "app",
			file: "public/bundle.js",
			globals: { zlib: "", "png-js": "", "jpeg-js": "", "pngJs": "", "jpegJs": "", crypto: "window.crypto" }
		},
		//     externals: ['zlib'],
		plugins: [

			gitVersion(),
// 			json(),
			// 		ignore(['zlib']),

			// Copy around the css files for Bulma
			//         postcss({
			//             extensions: [ '.css' ],
			//         }),

// 			image(),

			svelte({
				// enable run-time checks when not in production
				dev: !production,
				// we'll extract any component CSS out into
				// a separate file — better for performance
				css: (css) => {
					css.write("public/bundle.css");
				},
			}),

			// If you have external dependencies installed from
			// npm, you'll most likely need these plugins. In
			// some cases you'll need additional configuration —
			// consult the documentation for details:
			// https://github.com/rollup/rollup-plugin-commonjs
			resolve({
				preferBuiltins: true,
				browser: true,
			}),
			commonjs({
				//             exclude: ['node_modules/zlib/**']
				ignore: ["zlib", "url", "png-js", "jpeg-js", "crypto"],
			}),

			fileAsBlob({
				include: "src/*.svg"
			}),

// 			string({
// 				include: "src/*.svg"
// 			}),

			// If we're building for production (npm run build
			// instead of npm run dev), transpile and minify
			// 		production && buble({ include: ['src/**', 'node_modules/svelte/shared.js'] }),
			// 		production && uglify()
		],
	},


// 	{
// 		input: "src/TagEditorTestMain.js",
// 		output: {
// 			sourcemap: true,
// 			format: "iife",
// 			name: "app",
// 			file: "public/TagEditorTestBundle.js",
// 			globals: { zlib: "", "png-js": "", "jpeg-js": "" },
// 		},
// 		plugins: [
// 			svelte({
// 				// enable run-time checks when not in production
// 				dev: !production,
// 				// we'll extract any component CSS out into
// 				// a separate file — better for performance
// 				css: (css) => {
// 					css.write("public/TagEditorTestBundle.css");
// 				},
// 			}),
// 			resolve({
// 				preferBuiltins: true,
// 				browser: true,
// 			}),
// 			commonjs({
// 				ignore: ["zlib", "url"],
// 			}),
// 			fileAsBlob({
// 				include: "src/*.svg"
// 			}),
// 		],
// 	},
	

	{
		input: "src/externals-pouchdb.js",
		output: {
			sourcemap: false,
			format: "umd",
			file: "public/externals-pouchdb.js",
			name: "PouchDB",
		},
		plugins: [
			resolve({
				preferBuiltins: true,
				browser: true,
			}),
			commonjs({
				ignore: ["zlib", "url"],
			}),
		],
	},
	{
		input: "src/externals-pdfjs.js",
		output: {
			sourcemap: false,
			format: "umd",
			file: "public/externals-pdfjs.js",
			name: "PDFJS",
		},
		plugins: [
			resolve({
				preferBuiltins: true,
				browser: true,
			}),
			commonjs({
				ignore: ["zlib", "url"],
			}),
		],
	},
];
